FROM ubuntu:20.04
WORKDIR /src
ADD https://github.com/aquasecurity/trivy/releases/download/v0.17.2/trivy_0.17.2_Linux-64bit.deb /src
RUN dpkg -i trivy_0.17.2_Linux-64bit.deb
